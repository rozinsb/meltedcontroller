package config

import (
	"os"

	"github.com/Netflix/go-env"
	"gitlab.com/rozinsb/meltedcontroller/samm"
)

// Config holds application configuration data
type Config struct {
	ServiceName string `env:"SERVICE_NAME"`
	ServiceUUID string `env:"SERVICE_UUID"`
	ServiceHost string `env:"SERVICE_HOST"`

	NamespacePublisher string `env:"NAMESPACE_PUBLISHER"`
	MeltedEndpoint     string `env:"MELTED_ENDPOINT"`
	ChannelUUID        string `env:"CHANNEL_UUID"`
	PlayoutUUID        string `env:"PLAYOUT_UUID"`

	SammConfig *samm.Config
}

// GetConfig gets config from environment
func GetConfig() (*Config, error) {
	c := &Config{}
	_, err := env.UnmarshalFromEnviron(c)
	c.SammConfig = samm.DefaultConfig
	c.ServiceHost, _ = os.Hostname()
	return c, err
}
