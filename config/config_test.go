package config

import (
	"os"
	"testing"

	"github.com/stretchr/testify/require"

	"github.com/stretchr/testify/assert"
)

func TestGetConfig(t *testing.T) {
	os.Setenv("MELTED_ENDPOINT", "127.0.0.1:8000")
	os.Setenv("CHANNEL_UUID", "CHANNEL_UUID")

	c, err := GetConfig()
	require.NoError(t, err)
	assert.Equal(t, "127.0.0.1:8000", c.MeltedEndpoint)
	assert.Equal(t, "CHANNEL_UUID", c.ChannelUUID)
}
