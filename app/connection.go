package app

import (
	"fmt"

	"gitlab.com/rozinsb/meltedcontroller/config"
	"gitlab.com/rozinsb/meltedcontroller/melted"
)

type meltedConnectionWrapper struct {
	melted.Conn
	config     *config.Config
	activeConn melted.Conn
}

func (w *meltedConnectionWrapper) SendCommand(command *melted.Command) (*melted.Response, error) {
	if w.activeConn == nil {
		var err error
		w.activeConn, err = melted.Dial(w.config.MeltedEndpoint)
		if err != nil {
			return nil, fmt.Errorf("Can't connect to melted endpoint %s: %s", w.config.MeltedEndpoint, err)
		}
	}
	r, err := w.activeConn.SendCommand(command)
	if err != nil {
		w.activeConn = nil
	}
	return r, err
}

func (w *meltedConnectionWrapper) Close() error {
	if w.activeConn != nil {
		return w.activeConn.Close()
	}
	return nil
}
