package app

import (
	"bufio"
	"io"
	"net"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/rozinsb/meltedcontroller/config"
	"gitlab.com/rozinsb/meltedcontroller/melted"
)

func TestMeltedConnectionWrapper(t *testing.T) {
	endpoint := "localhost:7777"
	w := &meltedConnectionWrapper{
		config: &config.Config{
			MeltedEndpoint: endpoint,
		}}

	server := &fakeMeltedServer{}
	ready := make(chan struct{})
	go server.Listen(endpoint, ready)
	defer server.Shutdown()
	<-ready

	t.Run("Should establish connection on send", func(t *testing.T) {
		r, err := w.SendCommand(&melted.Command{Command: "cmd"})
		require.NoError(t, err)
		assert.Equal(t, 200, r.Code)
		assert.Equal(t, 1, server.ConnectionNum)
	})

	t.Run("Should establish connection on send", func(t *testing.T) {
		_, err := w.SendCommand(&melted.Command{Command: "error"})
		require.Error(t, err)
		assert.Equal(t, 1, server.ConnectionNum)
		t.Run("Should establish new connection after error", func(t *testing.T) {
			r, err := w.SendCommand(&melted.Command{Command: "cmd"})
			require.NoError(t, err)
			assert.Equal(t, 200, r.Code)
			assert.Equal(t, 2, server.ConnectionNum)
		})
	})
}

type fakeMeltedServer struct {
	listener      net.Listener
	ConnectionNum int
}

func (s *fakeMeltedServer) Listen(endpoint string, ready chan struct{}) {
	var err error
	s.listener, err = net.Listen("tcp", endpoint)

	ready <- struct{}{}

	if err != nil {
		panic(err)
	}

	for {
		conn, err := s.listener.Accept()
		if err != nil {
			return
		}
		s.ConnectionNum++
		go handleRequest(conn)
	}
}

func handleRequest(conn io.ReadWriteCloser) {
	defer conn.Close()
	l, _ := bufio.NewReader(conn).ReadString('\n')
	if strings.Contains(l, "error") {
		return
	}
	conn.Write([]byte("200 OK\n"))
}

func (s *fakeMeltedServer) Shutdown() {
	s.listener.Close()
}
