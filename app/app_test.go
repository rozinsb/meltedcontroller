package app

import (
	"bytes"
	"encoding/json"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/rozinsb/meltedcontroller/config"
	"gitlab.com/rozinsb/meltedcontroller/model"
	"gitlab.com/rozinsb/meltedcontroller/samm"
)

func TestService(t *testing.T) {
	endpoint := "localhost:9999"

	server := &fakeMeltedServer{}
	ready := make(chan struct{})
	go server.Listen(endpoint, ready)
	defer server.Shutdown()
	<-ready

	t.Run("Should run PLAY command", func(t *testing.T) {
		outputBuffer := &bytes.Buffer{}
		errorBuffer := &bytes.Buffer{}

		playCommand := `
		{
			"topic": "$NAMESPACE_SUBSCRIBER/cmd_play",
			"service_uuid": "$SERVICE_UUID",
			"service_name": "$SERVICE_NAME",
			"service_host": "$SERVICE_HOST",
			"created_at": "$CREATED_AT",
			"payload" : {
				"speed" : 1
			}
		  }
		`

		config := &config.Config{
			ServiceName: "ServiceName",
			ServiceUUID: "SERVICE_UUID",
			ServiceHost: "SERVICE_HOST",

			NamespacePublisher: "NAMESPACE_PUBLISHER",
			MeltedEndpoint:     endpoint,
			ChannelUUID:        "CHANNEL_UUID",
			PlayoutUUID:        "PLAYOUT_UUID",

			SammConfig: &samm.Config{
				Input:        strings.NewReader(playCommand),
				ObjectOutput: outputBuffer,
				ErrorOutput:  errorBuffer,
			},
		}
		service := NewService(config)
		service.Run()

		result := &samm.Message{}
		err := json.Unmarshal(outputBuffer.Bytes(), result)
		require.NoError(t, err)
		assert.Equal(t, config.ServiceHost, result.ServiceHost)
		assert.Equal(t, config.ServiceName, result.ServiceName)
		assert.Equal(t, config.ServiceUUID, result.ServiceUUID)
		assert.Equal(t, config.NamespacePublisher+"/cmd_response", result.Topic)

		commonResponse := &model.CommonResponse{}
		err = json.Unmarshal(result.Payload, commonResponse)

		require.NoError(t, err)
		assert.Equal(t, "PLAY u0 1", commonResponse.Command)
		assert.Equal(t, "200 OK", commonResponse.ResponseCode)
	})

	t.Run("Should call error handler on unknown command", func(t *testing.T) {
		outputBuffer := &bytes.Buffer{}
		errorBuffer := &bytes.Buffer{}

		playCommand := `
		{
			"topic": "$NAMESPACE_SUBSCRIBER/some_command",
			"service_uuid": "$SERVICE_UUID",
			"service_name": "$SERVICE_NAME",
			"service_host": "$SERVICE_HOST",
			"created_at": "$CREATED_AT",
			"payload" : {
				"speed" : 1
			}
		  }
		`

		config := &config.Config{
			ServiceName: "ServiceName",
			ServiceUUID: "SERVICE_UUID",
			ServiceHost: "SERVICE_HOST",

			NamespacePublisher: "NAMESPACE_PUBLISHER",
			MeltedEndpoint:     endpoint,
			ChannelUUID:        "CHANNEL_UUID",
			PlayoutUUID:        "PLAYOUT_UUID",

			SammConfig: &samm.Config{
				Input:        strings.NewReader(playCommand),
				ObjectOutput: outputBuffer,
				ErrorOutput:  errorBuffer,
			},
		}
		service := NewService(config)
		service.Run()

		result := &samm.ErrorMessage{}
		err := json.Unmarshal(errorBuffer.Bytes(), result)
		require.NoError(t, err)
		assert.True(t, strings.Contains(result.LogMessage, "some_command"))
	})
}
