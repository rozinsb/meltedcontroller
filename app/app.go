package app

import (
	"fmt"

	"gitlab.com/rozinsb/meltedcontroller/config"
	"gitlab.com/rozinsb/meltedcontroller/handler"
	"gitlab.com/rozinsb/meltedcontroller/melted"
	"gitlab.com/rozinsb/meltedcontroller/samm"
)

// Service main application engine
type Service struct {
	config *config.Config
	melted melted.Conn
	samm   samm.Client
}

// NewService creates main application object
func NewService(config *config.Config) Service {
	return Service{
		config: config,
		melted: &meltedConnectionWrapper{config: config},
		samm:   samm.NewClient(config.SammConfig)}
}

// Run runs application
func (s *Service) Run() {
	handler := &handler.CommandHandler{
		Config: s.config,
		Conn:   s.melted,
	}

	router := samm.NewRouter()
	router.SetErrorHandler(s.errorHandler)
	router.RegisterTopic("playlist_get", handler.HandleGetPlaylist)
	router.RegisterTopic("cmd_clip_load", handler.HandleLoadClip)
	router.RegisterTopic("cmd_clip_append", handler.HandleAppendClip)

	router.RegisterTopic("cmd_playlist_clean", handler.HandleClean)
	router.RegisterTopic("cmd_playlist_clear", handler.HandleClear)
	router.RegisterTopic("cmd_playlist_wipe", handler.HandleWipe)
	router.RegisterTopic("cmd_play", handler.HandlePlay)
	router.RegisterTopic("cmd_stop", handler.HandleStop)
	router.RegisterTopic("cmd_pause", handler.HandlePause)

	router.RegisterTopic("cmd_unit_status", handler.HandleUnitStatus)
	router.RegisterTopic("cmd_push_xml", handler.HandlePush)

	router.RegisterTopic("tick", handler.HandleTick)

	s.samm.Listen(router)
}

func (s *Service) errorHandler(command *samm.Message, writer samm.ResponseWriter) {
	writer.WriteError(fmt.Errorf("Can't process topic %s", command.Topic))
}
