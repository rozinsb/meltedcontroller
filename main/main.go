package main

import (
	"log"

	"gitlab.com/rozinsb/meltedcontroller/app"
	"gitlab.com/rozinsb/meltedcontroller/config"
)

func main() {
	cfg, err := config.GetConfig()
	if err != nil {
		log.Fatalf("Can't parse environment vars: %s", err)
		return
	}
	service := app.NewService(cfg)
	service.Run()
}
