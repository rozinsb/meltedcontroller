package model

// Playlist is a playlist
type Playlist struct {
	Generation int64           `json:"playlist_generation"`
	CreatedAt  string          `json:"created_at"`
	Items      []*PlaylistItem `json:"items"`
}

// PlaylistItem is a playlist item
type PlaylistItem struct {
	Index            int64  `json:"index"`
	Filename         string `json:"filename"`
	In               int64  `json:"in"`
	Out              int64  `json:"out"`
	Length           int64  `json:"length"`
	CalculatedLength int64  `json:"calculated_length"`
	FPS              string `json:"fps"`
}

// UnitStatusResponse response for unit status command
type UnitStatusResponse struct {
	PlayoutStatus PlayoutStatus `json:"playout_status"`
}

// PlayoutStatus is a playout status in unit status response
type PlayoutStatus struct {
	ChannelUUID     string `json:"channel_uuid"`
	PlayoutUUID     string `json:"playout_uuid"`
	Unit            int64  `json:"unit"`
	Mode            string `json:"mode"`
	CurrentClip     string `json:"current_clip"`
	CurrentPosition int64  `json:"current_position"`
	Speed           int64  `json:"speed"`
	Fps             string `json:"fps"`
	CurrentIn       int64  `json:"current_in"`
	CurrentOut      int64  `json:"current_out"`
	CurrentLength   int64  `json:"length"`
	BufferClip      string `json:"buffer_clip"`
	BufferPosition  int64  `json:"buffer_position"`
	BufferIn        int64  `json:"buffer_in"`
	BufferOut       int64  `json:"buffer_out"`
	BufferLength    int64  `json:"buffer_length"`
	Seekable        int64  `json:"seekable"`
	PlaylistNumber  int64  `json:"playlist_number"`
	ClipIndex       int64  `json:"clip_index"`
}

// CommonResponse response for WIPE, CLEAN, CLEAR, PLAY, STOP, PAUSE, LOAD, APND, PUSH
type CommonResponse struct {
	// Command is a stringed command, eg "apnd u0 filename"
	Command string `json:"command"`
	// ResponseCode is a full status response, eg "404 Failed to locate or open clip"
	ResponseCode string `json:"response_code"`
}
