package model

// LoadParams parameters for LOAD command
type LoadParams struct {
	FileName string `json:"filename"`
	In       *int64 `json:"in,omitempty"`
	Out      *int64 `json:"out,omitempty"`
}

// PlayParams parameters for PLAY command
type PlayParams struct {
	Speed *int `json:"speed,omitempty"`
}

// UnitStatusParams parameters for USTA command
type UnitStatusParams struct {
	TickUUID      string `json:"tick_uuid"`
	TickTimestamp string `json:"tick_timestamp"`
}

// PushParams parameters for PUSH command
type PushParams struct {
	XML string `json:"xml"`
}
