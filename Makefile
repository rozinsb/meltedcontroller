lint-prepare:
	go get -u github.com/golangci/golangci-lint/cmd/golangci-lint

lint:	
	golangci-lint run --no-config \
		--issues-exit-code=1 \
		--deadline=160s \
		--disable-all \
		--enable goconst \
		--enable golint \
		--enable gosimple \
		--enable maligned \
		--enable misspell \
		--enable ineffassign \
		--enable interfacer \
		--enable staticcheck \
		--enable structcheck \
		--enable unconvert \
		--enable varcheck \
		--enable gas

dep-restore:
	dep ensure

build:
	mkdir -p build
	go build -o build/app ./main/main.go

test: lint
	go test -cover ./...

deploy:
	docker-compose -f ./tools/docker-compose.yml build
	docker-compose -f ./tools/docker-compose.yml up
