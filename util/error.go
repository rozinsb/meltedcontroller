package util

import (
	"fmt"

	"gitlab.com/rozinsb/meltedcontroller/samm"
)

// LogAndRespond logs error and writes response to SAMM
func LogAndRespond(message string, err error, writer samm.ResponseWriter) {
	m := fmt.Sprintf("%s: %s", message, err)
	writer.WriteError(fmt.Errorf(m))
}
