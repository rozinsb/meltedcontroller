package samm

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestClient(t *testing.T) {
	t.Run("Should receive echo command response", func(t *testing.T) {
		outputBuffer := &bytes.Buffer{}
		errorBuffer := &bytes.Buffer{}

		message := &Message{Topic: "echo"}

		runSingleCommand(t, outputBuffer, errorBuffer, message)

		var result *Message
		err := json.Unmarshal(outputBuffer.Bytes(), &result)
		require.NoError(t, err)
		assert.Equal(t, message.Topic, result.Topic)
		assert.Empty(t, errorBuffer.String())
	})

	t.Run("Should receive error response", func(t *testing.T) {
		outputBuffer := &bytes.Buffer{}
		errorBuffer := &bytes.Buffer{}

		message := &Message{Topic: "error"}

		runSingleCommand(t, outputBuffer, errorBuffer, message)

		var result *ErrorMessage
		err := json.Unmarshal(errorBuffer.Bytes(), &result)
		require.NoError(t, err)
		assert.NotEmpty(t, result.LogMessage)
		assert.Empty(t, outputBuffer.String())
	})
}

func runSingleCommand(t *testing.T, objectOutput io.Writer, errorOutput io.Writer, message *Message) {
	b, err := json.Marshal(message)
	require.NoError(t, err)

	config := &Config{
		Input:        strings.NewReader(string(b)),
		ObjectOutput: objectOutput,
		ErrorOutput:  errorOutput,
	}

	client := NewClient(config)
	handler := &fakeHandler{}
	client.Listen(handler)
}

func createMockConfig() *Config {
	return &Config{}
}

type fakeHandler struct{}

func (h *fakeHandler) Handle(command *Message, writer ResponseWriter) {
	if command.Topic == "error" {
		writer.WriteError(fmt.Errorf("some error message"))
	} else {
		writer.WriteObject(command)
	}
}
