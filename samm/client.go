package samm

import (
	"encoding/json"
	"fmt"
	"io"
	"os"
)

// Config holds configuration for SAMM client
type Config struct {
	Input        io.Reader
	ObjectOutput io.Writer
	ErrorOutput  io.Writer
}

// DefaultConfig default configuration for SAMM client
var DefaultConfig = &Config{
	Input:        os.Stdin,
	ObjectOutput: os.Stdout,
	ErrorOutput:  os.Stderr,
}

// NewClient returns new SAMM client
func NewClient(config *Config) Client {
	return &client{config: config, writer: &responseWriter{config}}
}

type client struct {
	config *Config
	writer *responseWriter
}

func (c *client) Listen(handler CommandHandler) {
	decoder := json.NewDecoder(c.config.Input)
	for decoder.More() {
		var command Message
		err := decoder.Decode(&command)
		if err != nil {
			c.writer.WriteError(fmt.Errorf("Error while decoding input %s", err))
			return
		}

		handler.Handle(&command, c.writer)
	}
}
