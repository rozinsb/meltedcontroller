package samm

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestRouter(t *testing.T) {
	r := NewRouter()
	errorReceived := false
	echoProcessed := false
	r.RegisterTopic("echo", func(command *Message, writer ResponseWriter) {
		echoProcessed = true
	})
	r.SetErrorHandler(func(command *Message, writer ResponseWriter) {
		errorReceived = true
	})

	t.Run("Should handle echo topic", func(t *testing.T) {
		w := &responseWriter{config: DefaultConfig}
		c := &Message{Topic: "some_namespace/echo"}
		r.Handle(c, w)
		require.True(t, echoProcessed)
		assert.False(t, errorReceived)
	})

	t.Run("Should execute error handler", func(t *testing.T) {
		echoProcessed = false
		w := &responseWriter{config: DefaultConfig}
		c := &Message{Topic: "some_namespace/unknown"}
		r.Handle(c, w)
		require.True(t, errorReceived)
		assert.False(t, echoProcessed)
	})
}
