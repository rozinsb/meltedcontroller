package melted

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCommand_JoinArgs(t *testing.T) {
	c := &Command{
		Command: "some command",
		Arguments: []string{
			"1",
			"2 3",
			"4",
		},
	}

	assert.Equal(t, "1 \"2 3\" 4", c.JoinArgs())
}
