package melted

import (
	"bufio"
	"fmt"
	"io"
	"strconv"
	"strings"
)

type responseParser struct {
}

func (*responseParser) Parse(r io.Reader) (*Response, error) {
	reader := bufio.NewReader(r)
	header, _, err := reader.ReadLine()
	if err != nil {
		return nil, err
	}
	response, err := parseHeader(string(header))
	if err != nil {
		return nil, err
	}

	if response.Code == 201 {
		response.Payload, err = parseBody(reader)
		if err != nil {
			return nil, err
		}
		if len(response.Payload) == 0 {
			return nil, fmt.Errorf("Empty payload")
		}
	}

	if response.Code == 202 {
		singleLine, _, err := reader.ReadLine()
		if err != nil {
			return nil, err
		}
		response.Payload = []string{string(singleLine)}
	}

	return response, nil
}

func parseBody(reader *bufio.Reader) ([]string, error) {
	var lines []string

	for {
		line, _, err := reader.ReadLine()
		if err != nil {
			return nil, err
		}
		str := string(line)
		if str == "" {
			return lines, nil
		}
		lines = append(lines, string(line))
	}
}

func parseHeader(header string) (*Response, error) {

	delimIndex := strings.Index(header, " ")

	if delimIndex == -1 {
		return nil, fmt.Errorf("Can't parse status code from header %s", header)
	}

	status, err := strconv.ParseInt(header[:delimIndex], 10, 64)
	if err != nil {
		return nil, fmt.Errorf("Can't parse status code: %s is not a number", header[:delimIndex])
	}

	response := &Response{
		Code:   int(status),
		Status: header[delimIndex+1:],
	}

	return response, nil
}
