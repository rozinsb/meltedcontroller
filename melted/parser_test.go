package melted

import (
	"fmt"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestResponseParser_Parse(t *testing.T) {
	t.Run("Should parse single line response", func(t *testing.T) {
		reader := strings.NewReader("200 OK")
		parser := &responseParser{}
		response, err := parser.Parse(reader)
		require.NoError(t, err)
		assert.Equal(t, 200, response.Code)
		assert.Equal(t, "OK", response.Status)
	})

	t.Run("Should return error on incorrect input", func(t *testing.T) {
		reader := strings.NewReader("OK 200")
		parser := &responseParser{}
		_, err := parser.Parse(reader)
		require.Error(t, err)
	})
}

func TestResponseParser_Parse_WithBody(t *testing.T) {
	t.Run("Should parse single line body", func(t *testing.T) {
		payload := "some response"
		reader := strings.NewReader("202 OK\n" + payload + "\n")
		parser := &responseParser{}
		response, err := parser.Parse(reader)
		require.NoError(t, err)
		assert.Equal(t, 202, response.Code)
		require.Len(t, response.Payload, 1)
		assert.Equal(t, payload, response.Payload[0])
	})

	t.Run("Should parse multiline line body", func(t *testing.T) {
		payload1 := "some response 1"
		payload2 := "some response 2"
		reader := strings.NewReader(fmt.Sprintf("201 OK\n%s\n%s\n\n", payload1, payload2))
		parser := &responseParser{}
		response, err := parser.Parse(reader)
		require.NoError(t, err)
		assert.Equal(t, 201, response.Code)
		require.Len(t, response.Payload, 2)
		assert.Equal(t, payload1, response.Payload[0])
		assert.Equal(t, payload2, response.Payload[1])
	})
}

func TestResponseParser_Parse_WithInvalidBody(t *testing.T) {
	t.Run("Should return error when there is no body", func(t *testing.T) {
		reader := strings.NewReader("202 OK\n")
		parser := &responseParser{}
		_, err := parser.Parse(reader)
		require.Error(t, err)
	})
}
