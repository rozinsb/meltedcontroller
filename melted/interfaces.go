package melted

// Command is a command sent to Melted
type Command struct {
	Command   string
	Arguments []string
	Body      []string
}

// Response contains response data from melted
type Response struct {
	Code    int
	Status  string
	Payload []string
}

// Conn is a connection to Melted
type Conn interface {
	SendCommand(command *Command) (*Response, error)
	Close() error
}
