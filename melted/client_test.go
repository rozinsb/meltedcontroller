package melted

import (
	"bufio"
	"io"
	"net"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

const endpoint = "localhost:5555"

func TestClient(t *testing.T) {
	server := &fakeMeltedServer{}
	ready := make(chan struct{})
	defer server.Shutdown()
	go server.Listen(endpoint, ready)
	<-ready

	t.Run("Shoud dial and close connection", func(t *testing.T) {
		conn, err := Dial(endpoint)
		require.NoError(t, err)
		require.NoError(t, conn.Close())
	})

	t.Run("Shoud receive OK response on any command", func(t *testing.T) {
		conn, err := Dial(endpoint)
		require.NoError(t, err)
		defer conn.Close()

		response, err := conn.SendCommand(&Command{Command: "some command"})
		require.NoError(t, err)
		assert.Equal(t, 200, response.Code)
		assert.Equal(t, "OK", response.Status)
	})

	t.Run("Shoud receive multiline response", func(t *testing.T) {
		conn, err := Dial(endpoint)
		require.NoError(t, err)
		defer conn.Close()

		response, err := conn.SendCommand(&Command{Command: "multiline"})
		require.NoError(t, err)
		assert.Equal(t, 201, response.Code)
		assert.Equal(t, "OK", response.Status)
		assert.Len(t, response.Payload, 2)
		assert.Equal(t, "first", response.Payload[0])
		assert.Equal(t, "second", response.Payload[1])
	})
}

type fakeMeltedServer struct {
	listener net.Listener
}

func (s *fakeMeltedServer) Listen(endpoint string, ready chan struct{}) {
	var err error
	s.listener, err = net.Listen("tcp", endpoint)
	ready <- struct{}{}
	if err != nil {
		panic(err)
	}

	for {
		conn, err := s.listener.Accept()
		if err != nil {
			return
		}
		go handleRequest(conn)
	}
}

func handleRequest(conn io.ReadWriteCloser) {
	defer conn.Close()
	l, _ := bufio.NewReader(conn).ReadString('\n')
	if strings.Contains(l, "multiline") {
		conn.Write([]byte("201 OK\nfirst\nsecond\n\n"))
	} else {
		conn.Write([]byte("200 OK\n"))
	}
}

func (s *fakeMeltedServer) Shutdown() {
	s.listener.Close()
}
