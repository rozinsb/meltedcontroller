package melted

import (
	"bytes"
	"net"
	"time"
)

const operationTimeout = 10 * time.Second

// Dial connect to melted by specified endpoint
func Dial(endpoint string) (Conn, error) {
	c, err := net.Dial("tcp", endpoint)
	if err != nil {
		return nil, err
	}

	return &conn{conn: c, parser: &responseParser{}}, nil
}

type conn struct {
	conn   net.Conn
	parser *responseParser
}

func (c *conn) SendCommand(command *Command) (*Response, error) {
	buffer := bytes.NewBufferString(command.Command)
	args := command.JoinArgs()
	if args != "" {
		buffer.WriteString(" " + args)
	}
	buffer.WriteString("\n")

	for _, item := range command.Body {
		buffer.WriteString(item + "\n")
	}

	b := buffer.Bytes()
	bytesToSend := len(b)

	err := c.conn.SetWriteDeadline(time.Now().Add(operationTimeout))
	if err != nil {
		return nil, err
	}

	for bytesToSend > 0 {
		n, err := c.conn.Write(b)
		if err != nil {
			return nil, err
		}
		bytesToSend = bytesToSend - n
	}

	err = c.conn.SetReadDeadline(time.Now().Add(operationTimeout))
	if err != nil {
		return nil, err
	}

	return c.parser.Parse(c.conn)
}

func (c *conn) Close() error {
	return c.conn.Close()
}
