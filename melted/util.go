package melted

import (
	"fmt"
	"strings"
)

// JoinArgs quotes arguments containing whitespaces and joins them all in single string
func (c *Command) JoinArgs() string {
	checkedArgs := []string{}
	for i := range c.Arguments {
		if strings.Contains(c.Arguments[i], " ") {
			checkedArgs = append(checkedArgs, fmt.Sprintf("\"%s\"", c.Arguments[i]))
		} else {
			checkedArgs = append(checkedArgs, c.Arguments[i])
		}
	}

	return strings.Join(checkedArgs, " ")
}
