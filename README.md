## Running Metled controller
To run this setup just execute
    ```bash
    $ docker-compose -f ./tools/docker-compose.yml build
    $ docker-compose -f ./tools/docker-compose.yml up
    ```
