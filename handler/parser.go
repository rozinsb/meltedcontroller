package handler

import (
	"fmt"
	"strconv"

	shellwords "github.com/mattn/go-shellwords"
	"gitlab.com/rozinsb/meltedcontroller/melted"
	"gitlab.com/rozinsb/meltedcontroller/model"
)

// ParsePlaylist parses playlist from melted response
func ParsePlaylist(r *melted.Response) (*model.Playlist, error) {
	playlist := &model.Playlist{
		Items: []*model.PlaylistItem{},
	}

	var err error
	playlist.Generation, err = strconv.ParseInt(r.Payload[0], 10, 64)
	if err != nil {
		return nil, fmt.Errorf("Can't parse generation: %s", err)
	}

	for _, item := range r.Payload[1:] {
		if item == "" {
			return playlist, nil
		}
		split, err := shellwords.Parse(item)
		if err != nil {
			return nil, err
		}
		playlistItem := &model.PlaylistItem{}

		if err := parceInt64AndCheck("item index", split[0], &playlistItem.Index); err != nil {
			return nil, err
		}

		playlistItem.Filename = split[1]

		if err := parceInt64AndCheck("in", split[2], &playlistItem.In); err != nil {
			return nil, err
		}
		if err := parceInt64AndCheck("out", split[3], &playlistItem.Out); err != nil {
			return nil, err
		}
		if err := parceInt64AndCheck("length", split[4], &playlistItem.Length); err != nil {
			return nil, err
		}
		if err := parceInt64AndCheck("calculated length", split[5], &playlistItem.CalculatedLength); err != nil {
			return nil, err
		}
		playlistItem.FPS = split[6]

		playlist.Items = append(playlist.Items, playlistItem)
	}

	return playlist, nil
}

// ParsePlayoutStatus parses playout status from melted response
func ParsePlayoutStatus(r *melted.Response) (*model.UnitStatusResponse, error) {
	result := &model.UnitStatusResponse{}

	split, err := shellwords.Parse(r.Payload[0])
	if err != nil {
		return nil, err
	}

	if err := parceInt64AndCheck("unit", split[0], &result.PlayoutStatus.Unit); err != nil {
		return nil, err
	}

	result.PlayoutStatus.Mode = split[1]
	result.PlayoutStatus.CurrentClip = split[2]
	if err := parceInt64AndCheck("current_position", split[3], &result.PlayoutStatus.CurrentPosition); err != nil {
		return nil, err
	}
	if err := parceInt64AndCheck("speed", split[4], &result.PlayoutStatus.Speed); err != nil {
		return nil, err
	}
	result.PlayoutStatus.Fps = split[5]
	if err := parceInt64AndCheck("current_in", split[6], &result.PlayoutStatus.CurrentIn); err != nil {
		return nil, err
	}
	if err := parceInt64AndCheck("current_out", split[7], &result.PlayoutStatus.CurrentOut); err != nil {
		return nil, err
	}
	if err := parceInt64AndCheck("current_length", split[8], &result.PlayoutStatus.CurrentLength); err != nil {
		return nil, err
	}

	result.PlayoutStatus.BufferClip = split[9]
	if err := parceInt64AndCheck("buffer_position", split[10], &result.PlayoutStatus.BufferPosition); err != nil {
		return nil, err
	}
	if err := parceInt64AndCheck("buffer_in", split[11], &result.PlayoutStatus.BufferIn); err != nil {
		return nil, err
	}
	if err := parceInt64AndCheck("buffer_out", split[12], &result.PlayoutStatus.BufferOut); err != nil {
		return nil, err
	}
	if err := parceInt64AndCheck("buffer_length", split[13], &result.PlayoutStatus.BufferLength); err != nil {
		return nil, err
	}
	if err := parceInt64AndCheck("seekable", split[14], &result.PlayoutStatus.Seekable); err != nil {
		return nil, err
	}
	if err := parceInt64AndCheck("playlist_number", split[15], &result.PlayoutStatus.PlaylistNumber); err != nil {
		return nil, err
	}
	if err := parceInt64AndCheck("clip_index", split[16], &result.PlayoutStatus.ClipIndex); err != nil {
		return nil, err
	}

	return result, nil
}

func parceInt64AndCheck(elementName string, data string, result *int64) error {
	var err error
	*result, err = strconv.ParseInt(data, 10, 64)
	if err != nil {
		return fmt.Errorf("Can't parse %s: %s", elementName, err)
	}
	return nil
}
