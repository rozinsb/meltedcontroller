package handler

import (
	"encoding/json"
	"fmt"
	"regexp"
	"strconv"
	"time"

	"gitlab.com/rozinsb/meltedcontroller/config"
	"gitlab.com/rozinsb/meltedcontroller/melted"
	"gitlab.com/rozinsb/meltedcontroller/model"
	"gitlab.com/rozinsb/meltedcontroller/samm"
	"gitlab.com/rozinsb/meltedcontroller/util"
)

const defaultUnit = "u0"

// CommandHandler handles application commands to melted server
type CommandHandler struct {
	Config *config.Config
	Conn   melted.Conn
}

// HandleGetPlaylist gets playlist
func (c *CommandHandler) HandleGetPlaylist(message *samm.Message, writer samm.ResponseWriter) {
	r, err := c.Conn.SendCommand(&melted.Command{Command: "LIST", Arguments: []string{defaultUnit}})
	if e := c.checkResponseAndError(r, err); e != nil {
		util.LogAndRespond("Can't get playlist", e, writer)
		return
	}

	playlist, err := ParsePlaylist(r)
	if err != nil {
		util.LogAndRespond("Can't parse playlist", err, writer)
		return
	}

	playlist.CreatedAt = time.Now().Format(samm.TimeFormat)

	response, err := c.makeResponse("playlist", playlist, playlist.CreatedAt)
	if err != nil {
		util.LogAndRespond("Can't parse playlist", err, writer)
		return
	}
	writer.WriteObject(response)
}

// HandleLoadClip loads clip into unit
func (c *CommandHandler) HandleLoadClip(message *samm.Message, writer samm.ResponseWriter) {
	params := &model.LoadParams{}
	err := json.Unmarshal(message.Payload, params)
	if err != nil {
		util.LogAndRespond("Can't parse parameters for LOAD command", err, writer)
		return
	}

	p := []string{params.FileName}

	if params.In != nil {
		p = append(p, strconv.FormatInt(*params.In, 10))
	}
	if params.Out != nil {
		p = append(p, strconv.FormatInt(*params.Out, 10))
	}
	c.handleSingleUnitCommandWithParams(writer, "cmd_response", "LOAD", p)
}

// HandleAppendClip appends clip into unit
func (c *CommandHandler) HandleAppendClip(message *samm.Message, writer samm.ResponseWriter) {
	params := &model.LoadParams{}
	err := json.Unmarshal(message.Payload, params)
	if err != nil {
		util.LogAndRespond("Can't parse parameters for APND command", err, writer)
		return
	}

	p := []string{params.FileName}

	if params.In != nil {
		p = append(p, strconv.FormatInt(*params.In, 10))
	}
	if params.Out != nil {
		p = append(p, strconv.FormatInt(*params.Out, 10))
	}
	c.handleSingleUnitCommandWithParams(writer, "cmd_response", "APND", p)
}

// HandleClean performs CLEAN command
func (c *CommandHandler) HandleClean(message *samm.Message, writer samm.ResponseWriter) {
	c.handleSingleUnitCommand(writer, "cmd_response", "CLEAN")
}

// HandleClear performs CLEAR command
func (c *CommandHandler) HandleClear(message *samm.Message, writer samm.ResponseWriter) {
	c.handleSingleUnitCommand(writer, "cmd_response", "CLEAR")
}

// HandleWipe performs WIPE command
func (c *CommandHandler) HandleWipe(message *samm.Message, writer samm.ResponseWriter) {
	c.handleSingleUnitCommand(writer, "cmd_response", "WIPE")
}

// HandlePlay performs PLAY command
func (c *CommandHandler) HandlePlay(message *samm.Message, writer samm.ResponseWriter) {
	params := &model.PlayParams{}
	err := json.Unmarshal(message.Payload, params)
	if err != nil {
		util.LogAndRespond("Can't parse parameters for PLAY command", err, writer)
		return
	}

	p := []string{}
	if params.Speed != nil {
		p = append(p, strconv.Itoa(*params.Speed))
	}

	c.handleSingleUnitCommandWithParams(writer, "cmd_response", "PLAY", p)
}

// HandleStop performs STOP command
func (c *CommandHandler) HandleStop(message *samm.Message, writer samm.ResponseWriter) {
	c.handleSingleUnitCommand(writer, "cmd_response", "STOP")
}

// HandlePause performs PAUSE command
func (c *CommandHandler) HandlePause(message *samm.Message, writer samm.ResponseWriter) {
	c.handleSingleUnitCommand(writer, "cmd_response", "PAUSE")
}

// HandleUnitStatus gets unit status
func (c *CommandHandler) HandleUnitStatus(message *samm.Message, writer samm.ResponseWriter) {
	r, err := c.Conn.SendCommand(&melted.Command{Command: "USTA", Arguments: []string{defaultUnit}})
	if e := c.checkResponseAndError(r, err); e != nil {
		util.LogAndRespond("Can't get unit status", e, writer)
		return
	}

	status, err := ParsePlayoutStatus(r)
	if err != nil {
		util.LogAndRespond("Can't parse playout response", err, writer)
		return
	}

	status.PlayoutStatus.ChannelUUID = c.Config.ChannelUUID
	status.PlayoutStatus.PlayoutUUID = c.Config.PlayoutUUID

	response, err := c.makeResponse(
		fmt.Sprintf("/channel/%s/playout/%s/status", c.Config.ChannelUUID, c.Config.PlayoutUUID),
		status)
	if err != nil {
		util.LogAndRespond("Can't marshal playout status", err, writer)
		return
	}
	writer.WriteObject(response)
}

// HandlePush performs push command
func (c *CommandHandler) HandlePush(message *samm.Message, writer samm.ResponseWriter) {
	params := &model.PushParams{}
	err := json.Unmarshal(message.Payload, params)
	if err != nil {
		util.LogAndRespond("Can't get parse parameters for PUSH command", err, writer)
		return
	}

	re := regexp.MustCompile(`\r?\n`)
	xml := re.ReplaceAllString(params.XML, " ")

	// +1 because when body processed there is endline symbol at the end
	p := []string{strconv.Itoa(len(xml) + 1), xml}
	c.handleSingleUnitCommandWithBody(writer, "cmd_response", "PUSH", p)
}

// HandleTick handles tick topic for debug purposes
func (c *CommandHandler) HandleTick(message *samm.Message, writer samm.ResponseWriter) {
	response := &samm.Message{
		Topic:       "default/tick_response",
		ServiceUUID: c.Config.ServiceUUID,
		ServiceName: c.Config.ServiceName,
		ServiceHost: c.Config.ServiceHost,
		CreatedAt:   time.Now().Format(samm.TimeFormat),
	}
	writer.WriteObject(response)
}

func (c *CommandHandler) handleSingleUnitCommand(writer samm.ResponseWriter, resultTopic string, command string) {
	c.handleSingleUnitCommandWithParams(writer, resultTopic, command, []string{})
}

func (c *CommandHandler) handleSingleUnitCommandWithParams(writer samm.ResponseWriter, resultTopic string, command string, params []string) {
	c.generalHandleSingleUnitCommand(writer, resultTopic, command, params, false)
}

func (c *CommandHandler) handleSingleUnitCommandWithBody(writer samm.ResponseWriter, resultTopic string, command string, params []string) {
	c.generalHandleSingleUnitCommand(writer, resultTopic, command, params, true)
}

// generalHandleSingleUnitCommand processes simple command with params
func (c *CommandHandler) generalHandleSingleUnitCommand(writer samm.ResponseWriter, resultTopic string, command string, params []string, inBody bool) {
	cmd := &melted.Command{
		Command:   command,
		Arguments: []string{defaultUnit}}

	if inBody {
		if len(params) > 0 {
			cmd.Body = append(cmd.Body, params...)
		}
	} else {
		if len(params) > 0 {
			cmd.Arguments = append(cmd.Arguments, params...)
		}
	}

	r, err := c.Conn.SendCommand(cmd)
	if err != nil {
		util.LogAndRespond("Can't perform "+command+" command", err, writer)
		return
	}

	payload := &model.CommonResponse{
		Command:      fmt.Sprintf("%s %s", cmd.Command, cmd.JoinArgs()),
		ResponseCode: fmt.Sprintf("%d %s", r.Code, r.Status),
	}

	response, err := c.makeResponse(resultTopic, payload)
	if err != nil {
		util.LogAndRespond("Can't marshal "+command+" params", err, writer)
		return
	}
	writer.WriteObject(response)
}

func (c *CommandHandler) checkResponseAndError(response *melted.Response, err error) error {
	if err != nil {
		return err
	}
	if response.Code < 200 || response.Code >= 300 {
		return fmt.Errorf("Bad status code %d, status: %s", response.Code, response.Status)
	}
	return nil
}

func (c *CommandHandler) makeResponse(topic string, payload interface{}, overrideTime ...string) (*samm.Message, error) {
	finalTopic := topic
	if c.Config.NamespacePublisher != "" {
		finalTopic = c.Config.NamespacePublisher + "/" + topic
	}
	response := &samm.Message{
		Topic:       finalTopic,
		ServiceUUID: c.Config.ServiceUUID,
		ServiceName: c.Config.ServiceName,
		ServiceHost: c.Config.ServiceHost,
		CreatedAt:   time.Now().Format(samm.TimeFormat),
	}

	if len(overrideTime) > 0 {
		response.CreatedAt = overrideTime[0]
	}

	if payload != nil {
		var err error
		response.Payload, err = json.Marshal(payload)
		if err != nil {
			return nil, err
		}
	}

	return response, nil
}
