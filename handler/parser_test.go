package handler

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/rozinsb/meltedcontroller/melted"
)

func TestParsePlayoutStatus(t *testing.T) {
	r := &melted.Response{
		Payload: []string{"0 playing \"/videos/cherrybomb_wu_F.mov\" 1051 1000 25.00 0 5206 5207 \"/videos/cherrybomb_wu_F.mov\" 1051 0 5206 5207 1 4 3"},
	}
	t.Run("Should parse playout status", func(t *testing.T) {
		s, err := ParsePlayoutStatus(r)
		require.NoError(t, err)
		assert.Equal(t, int64(0), s.PlayoutStatus.Unit)
		assert.Equal(t, "playing", s.PlayoutStatus.Mode)
		assert.Equal(t, "/videos/cherrybomb_wu_F.mov", s.PlayoutStatus.CurrentClip)
		assert.Equal(t, int64(1051), s.PlayoutStatus.CurrentPosition)
		assert.Equal(t, int64(1000), s.PlayoutStatus.Speed)
		assert.Equal(t, "25.00", s.PlayoutStatus.Fps)
		assert.Equal(t, int64(0), s.PlayoutStatus.CurrentIn)
		assert.Equal(t, int64(5206), s.PlayoutStatus.CurrentOut)
		assert.Equal(t, int64(5207), s.PlayoutStatus.CurrentLength)
		assert.Equal(t, "/videos/cherrybomb_wu_F.mov", s.PlayoutStatus.BufferClip)
		assert.Equal(t, int64(1051), s.PlayoutStatus.BufferPosition)
		assert.Equal(t, int64(0), s.PlayoutStatus.BufferIn)
		assert.Equal(t, int64(5206), s.PlayoutStatus.BufferOut)
		assert.Equal(t, int64(5207), s.PlayoutStatus.BufferLength)
		assert.Equal(t, int64(1), s.PlayoutStatus.Seekable)
		assert.Equal(t, int64(4), s.PlayoutStatus.PlaylistNumber)
		assert.Equal(t, int64(3), s.PlayoutStatus.ClipIndex)
	})
}

func TestParsePlaylist(t *testing.T) {
	r := &melted.Response{
		Payload: []string{
			"789",
			"0 \"filename 1\" 0 1234 1235 1235 25.00",
			"1 filename 0 1234 1235 1235 25.00",
		},
	}
	t.Run("Should parse playlist", func(t *testing.T) {
		l, err := ParsePlaylist(r)
		require.NoError(t, err)
		assert.Equal(t, int64(789), l.Generation)
		assert.Len(t, l.Items, 2)
		assert.Equal(t, int64(0), l.Items[0].Index)
		assert.Equal(t, "filename 1", l.Items[0].Filename)
		assert.Equal(t, int64(0), l.Items[0].In)
		assert.Equal(t, int64(1234), l.Items[0].Out)
		assert.Equal(t, int64(1235), l.Items[0].Length)
		assert.Equal(t, int64(1235), l.Items[0].CalculatedLength)
		assert.Equal(t, "25.00", l.Items[0].FPS)

		assert.Equal(t, int64(1), l.Items[1].Index)
		assert.Equal(t, "filename", l.Items[1].Filename)
		assert.Equal(t, int64(0), l.Items[1].In)
		assert.Equal(t, int64(1234), l.Items[1].Out)
		assert.Equal(t, int64(1235), l.Items[1].Length)
		assert.Equal(t, int64(1235), l.Items[1].CalculatedLength)
		assert.Equal(t, "25.00", l.Items[1].FPS)
	})
}
